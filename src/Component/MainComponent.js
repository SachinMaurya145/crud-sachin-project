
import React, { useEffect, useState } from "react";

const MainComponent = () => {

    const [ApiData, setApiData] = useState([]);
    const [newData, setNewData] = useState('');

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => setApiData(data))
    }, [])

    const handleAddButton = (e) => {
        e.preventDefault();

        fetch('https://jsonplaceholder.typicode.com/users', {

            method: "POST",
            body: JSON.stringify({ name: newData }),
            headers: {
                "Content-Type": 'application/json'
            }

        }).then(response => response.json())
            .then(data => setApiData([...ApiData, data]))
        setNewData('');
    }

    const handleDeleteButton = (id) => {
        console.log(id);

        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,
            { method: 'DELETE' })
            .then(() => console.log('Delete successful'));
    }

    return (
        <div>
            <h2> CRUD </h2>
            <div>
                <input type="text" value={newData} onChange={(e) => setNewData(e.target.value)} />
                <button onClick={handleAddButton}> Add New Name</button>
            </div>
            <ul>
                {
                    ApiData?.map((val) => (
                        <li key={val.id}>
                            <div style={{ cursor: 'pointer' }}>
                                {val.name}
                                <button type="button" onClick={() => handleDeleteButton(val.id)} >delete </button>

                            </div>

                        </li>
                    ))
                }
            </ul>

        </div>
    )

}

export default MainComponent;